package practice7.mappers;

import practice7.helpers.ValueHolder;
import practice7.mappers.api.SessionMappeble;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by lex on 19.09.16.
 */
public class SessionModelMapper {
    public static <T extends SessionMappeble> T sessionObjMap(SessionMappeble session, Class<T> toClass) {
        T result = null;
        ValueHolder holder;
        try {
            result = toClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        result.setId(session.getId());
        holder = new ValueHolder();
        holder.setId(session.getFilmAsEntity().getId());
        result.setFilmAsEntity(holder);
        result.setDateOfSeance(LocalDateTime.parse(session.getDateOfSeance().toString()));
        holder = new ValueHolder();
        holder.setId(session.getHallAsEntity().getId());
        result.setHallAsEntity(holder);
        result.setPrice(new BigDecimal(session.getPrice().toString()));
        return result;
    }

    public static <E extends SessionMappeble, T extends SessionMappeble> List<T>
    sessionListMap(Iterable<E> entitys, Class<T> toClass) {
        List<T> result = new LinkedList<T>();
        for (E e : entitys) {
            result.add(sessionObjMap(e, toClass));
        }
        return result;
    }
}
