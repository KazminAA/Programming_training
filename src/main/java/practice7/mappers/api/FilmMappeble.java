package practice7.mappers.api;

/**
 * Created by lex on 19.09.16.
 */
public interface FilmMappeble {
    int getId();

    void setId(int id);

    int getYearOfRelease();

    void setYearOfRelease(int yearOfRelease);

    String getName();

    void setName(String name);

    String getGenre();

    void setGenre(String genre);

    int getDurationMin();

    void setDurationMin(int minutes);

    void setDurationMin(int hours, int minutes);

    String getCountry();

    void setCountry(String country);

    String getProduser();

    void setProduser(String produser);

    String getCast();

    void setCast(String cast);

    public String getDescription();

    void setDescription(String description);

}