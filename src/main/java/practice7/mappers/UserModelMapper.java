package practice7.mappers;

import practice7.mappers.api.UserMappebel;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Alexandr on 20.09.2016.
 */
public class UserModelMapper {
    public static <T extends UserMappebel> T userObjMap(UserMappebel userMappebel, Class<T> toClass) {
        T result = null;
        try {
            result = toClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        result.setId(userMappebel.getId());
        result.setLogin(userMappebel.getLogin());
        result.setPwd(userMappebel.getPwd());
        result.setUserName(userMappebel.getUserName());
        result.setUserSurname(userMappebel.getUserSurname());
        //result.setBirthday(LocalDate.parse(userMappebel.getBirthday().toString()));
        result.setRole(userMappebel.getRole());
        result.setSex(userMappebel.getSex());
        return result;
    }

    public static <E extends UserMappebel, T extends UserMappebel> List<T>
    userListMap(Iterable<E> entitys, Class<T> toClass) {
        List<T> result = new LinkedList<T>();
        for (E e : entitys) {
            result.add(userObjMap(e, toClass));
        }
        return result;
    }
}
