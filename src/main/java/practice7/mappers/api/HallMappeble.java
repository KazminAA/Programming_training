package practice7.mappers.api;

/**
 * Created by lex on 19.09.16.
 */
public interface HallMappeble {
    int getId();

    void setId(int id);

    String getName();

    void setName(String name);

    int[] getStructure();

    void setStructure(int[] structure);
}
