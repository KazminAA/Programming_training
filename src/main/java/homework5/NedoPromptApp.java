package homework5;

import homework1.UncloseInputStream;

import java.io.*;

/**
 * Created by Alex on 23.08.2016.
 */
public class NedoPromptApp {
    public static void main(String[] args) throws IOException {
        NedoPrompt np = new NedoPrompt();
        String file;
        String transWay;
        while (true) {
            System.out.print("Enter pathname of file to translate:");
            try (BufferedReader br = new BufferedReader(new InputStreamReader(new UncloseInputStream(System.in)))) {
                file = br.readLine();
                System.out.print("Enter way to translate.");
                transWay = br.readLine();
                file = np.translateText(file, transWay);
                break;
            } catch (IOException e) {
                System.out.println();
                System.out.println("Wrong filename!");
            }
        }
        System.out.println(file);
    }
}
