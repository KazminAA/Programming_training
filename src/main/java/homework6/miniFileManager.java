package homework6;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alex on 29.08.2016.
 */
public class miniFileManager {

    public static void mkDir(String name) {
        File f = new File(name);
        if (name.matches(".+\\.+") || name.matches(".+/.+")) {
            f.mkdirs();
        } else {
            f.mkdir();
        }
    }

    public static void createFile(String name) {
        File f = new File(name);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                System.out.println("Can't create file. Check the filename.");
            }
        } else {
            System.out.println("File " + name + " already exists.");
        }
    }

    public static void deleteFile(String name) {
        File f = new File(name);

        if (f.exists()) {
            f.delete();
        } else {
            System.out.println("Can't find such file or directory");
        }
    }

    public static void forcedelete(String name) {
        File f = new File(name);
        if (!f.exists()) {
            System.out.println("Can't find such file or directory.");
        }
        if (f.isDirectory()) {
            File[] fs = f.listFiles();
            for (int i = 0; i < fs.length; i++) {
                forcedelete(fs[i].getPath());
            }
        }
        f.delete();
    }

    public static void renameFile(String oldFilename, String newFilename) {
        File f1 = new File(oldFilename);
        String pattern = null;
        if (!newFilename.matches("(.+\\\\).+") || !newFilename.matches("(.+/).+")) {
            if (oldFilename.matches("(.+\\\\).+")) {
                pattern = "(.+\\\\).+";
            }
            if (oldFilename.matches("(.+/).+")) {
                pattern = "(.+/).+";
            }
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(oldFilename);
            if (m.find()) {
                newFilename = m.group(1) + newFilename;
            }
        }
        File f2 = new File(newFilename);
        if (f1.exists()) {
            f1.renameTo(f2);
        } else {
            System.out.println("Can't find such file or directory");
        }
    }

    public static void dirList(String name) {
        File f = new File(name);
        if (f.exists()) {
            if (f.isDirectory()) {
                File[] fs = f.listFiles();
                for (File file : fs) {
                    System.out.println(file.getName());
                }
            } else {
                System.out.println("Not a directory.");
            }
        } else {
            System.out.println("Can't find such file or directory");
        }
    }
}
