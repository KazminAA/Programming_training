package homework6;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alex on 29.08.2016.
 */
public class Book implements Serializable {
    private String title;
    private String author;
    private int year;

    public Book(String title, String author, int year) {
        setTitle(title);
        setAuthor(author);
        setYear(year);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", year=" + year +
                '}';
    }

    public static List<Book> createBooksFromFile(String fileName) {
        List<Book> booksTemp = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String str;
            Pattern p = Pattern.compile("(.+?);(.+?);(\\d+)");
            Matcher m;
            while ((str = br.readLine()) != null) {
                m = p.matcher(str);
                if (m.find()) {
                    booksTemp.add(new Book(m.group(1), m.group(2), Integer.parseInt(m.group(3))));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return booksTemp;
    }

    public void bookToFile(String fileName) throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
        out.writeObject(this);
        out.close();
    }
}
