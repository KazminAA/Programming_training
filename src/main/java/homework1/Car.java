package homework1;

/**
 * Created by lex on 15.07.16.
 */
public class Car {
    private String mark;
    private String model;
    private int year;

    public Car(String mark, String model, int year) {
        setMark(mark);
        setModel(model);
        setYear(year);
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

}
