package homework7.part3;

/**
 * Created by Alexandr on 06.09.2016.
 */
public class Counter {
    private int counter1;
    private int counter2;

    public Counter(int counter1, int counter2) {
        setCounter1(counter1);
        setCounter2(counter2);
    }

    public void incrementC1() {
        this.counter1++;
    }

    public void incrementC2() {
        this.counter2++;
    }

    public int getCounter1() {
        return counter1;
    }

    public void setCounter1(int counter1) {
        this.counter1 = counter1;
    }

    public int getCounter2() {
        return counter2;
    }

    public void setCounter2(int counter2) {
        this.counter2 = counter2;
    }
}
