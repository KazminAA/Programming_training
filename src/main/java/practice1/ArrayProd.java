package practice1;

/**
 * Created by lex on 12.07.16.
 */
public class ArrayProd {
    public static double arrayProd(double[] dArray) {
        double prod = 1;
        for (double d : dArray) {
            prod *= d;
        }
        return prod;
    }
}
