package practice4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by lex on 19.08.16.
 */
public class StudentUtils {
    public static Map<String, Student> createMapFromList(List<Student> students) {
        Map<String, Student> studentMap = new HashMap<>();
        for (int i = 0; i < students.size(); i++) {
            String key = students.get(i).getName().concat(students.get(i).getSuname());
            studentMap.put(key, students.get(i));
        }
        return studentMap;
    }

    public static void studentPrint(List<Student> students, int course) {
        Iterator<Student> stIterator = students.iterator();
        while (stIterator.hasNext()) {
            Student student = stIterator.next();
            if (student.getCourse() == course) {
                System.out.println(student);
            }
        }
    }

    public static List<Student> studentSort(List<Student> students) {
        students.sort((o1, o2) -> o1.getSuname().concat(o1.getName()).compareTo(o2.getSuname().concat(o2.getName())));
        return students;
    }
}
