package homework5;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alex on 23.08.2016.
 */
public class NedoPrompt {

    private Map<String, Map<String, String>> dicts; //container for dictionaries

    public NedoPrompt() throws IOException {
        setDicts(makeDictionaries());
    }

    public Map<String, Map<String, String>> getDicts() {
        return dicts;
    }

    private void setDicts(Map<String, Map<String, String>> dicts) {
        this.dicts = dicts;
    }

    /**
     * Method to load and parse dictionaries. The file os dictionaries must contain text like "word \s translation"
     *
     * @param file file to load
     * @return map that keys equals words and values equals word translation
     * @throws IOException
     */

    private Map<String, String> loadDictFromFile(File file) throws IOException {
        Map<String, String> dictionary = new HashMap<>();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String word;
        while ((word = br.readLine()) != null) {
            Pattern p = Pattern.compile("(.*)\\s(.*)");
            Matcher m = p.matcher(word);
            if (m.find()) {
                dictionary.put(m.group(1), m.group(2));
            }
        }
        return dictionary;
    }

    /**
     * Metod make dictionaries by load it from directory and save to container. The name of dictionary file must
     * equals translation way (like \"language translate from"-"language translate to"
     * @return dictionaries container
     * @throws IOException
     */

    private Map<String, Map<String, String>> makeDictionaries() throws IOException {
        File dir = new File("Dictionaries");
        Map<String, Map<String, String>> dicts = new HashMap<>();
        if (dir.exists()) {
            File[] dictionaries = dir.listFiles();
            String tmpStr;
            for (int i = 0; i < dictionaries.length; i++) {
                tmpStr = dictionaries[i].getName();
                Pattern p = Pattern.compile("([\\s\\S]+?).txt?");
                Matcher m = p.matcher(tmpStr);
                tmpStr = m.replaceAll("$1");
                System.out.println(tmpStr);
                dicts.put(tmpStr, loadDictFromFile(dictionaries[i]));
            }
        } else {
            System.out.println("Can't finde dictionaries directory.");
        }
        return dicts;
    }

    /**
     * Method that translate text by use dictionaries
     *
     * @param filename file with text to translate
     * @param transway way of translation must match the name of the dictionary without the file extension
     * @return String that contains original text and it translation
     * @throws IOException
     */

    public String translateText(String filename, String transway) throws IOException {
        Map<String, String> dict;
        if ((dict = getDicts().get(transway.toLowerCase())) == null) {
            throw new IllegalArgumentException("No such Dictionary.");
        }
        String strToTranslate;
        String tmpTransWord;
        StringBuilder sb = new StringBuilder();
        BufferedReader br1 = new BufferedReader(new FileReader(new File(filename)));
        while ((strToTranslate = br1.readLine()) != null) {
            sb.append(strToTranslate);
            sb.append('\n');
        }
        strToTranslate = sb.toString();
        sb.append("Translation~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        sb.append('\n');
        Pattern p = Pattern.compile("([^\\s,.!?]+)([\\s|,|.|!|?]\\n*)");
        Matcher m = p.matcher(strToTranslate);
        while (m.find()) {
            if (m.group(1).equals("a") || m.group(1).equals("the") || m.group(1).equals("are")) {
                sb.append("");
            } else {
                //if word do not finded in dictionary it past to translation text like it is
                if ((tmpTransWord = dict.get(m.group(1).toLowerCase())) != null) {
                    sb.append(tmpTransWord);
                } else {
                    sb.append(m.group(1));
                }
                sb.append(m.group(2));
            }
        }
        strToTranslate = sb.toString();
        return strToTranslate;
    }
}
