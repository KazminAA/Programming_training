package practice2;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by lex on 05.08.16.
 */
public class ParserTest {
    public static void main(String[] args) throws IOException {
        ArrayList<NameRating> nameRatings = ParseHtmlUtils.babyNameRatingParser("baby2008.html", "utf-8");
        for (int i = 0; i < 5; i++) {
            System.out.println(nameRatings.get(i));
        }
        ArrayList<Notebook> notebooks = ParseHtmlUtils.notebooksPriceParser("source.html", "windows-1251");
        for (int i = 0; i < 5; i++) {
            System.out.println(notebooks.get(i));
        }
    }
}
