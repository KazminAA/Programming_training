package practice5;

import java.io.Serializable;

/**
 * Created by Alexandr on 31.08.2016.
 */
public class Student implements Serializable {
    private String name;
    private int year;
    private int avgRaiting;

    public Student(String name, int year, int avgRaiting) {
        setName(name);
        setYear(year);
        setAvgRaiting(avgRaiting);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getAvgRaiting() {
        return avgRaiting;
    }

    public void setAvgRaiting(int avgRaiting) {
        this.avgRaiting = avgRaiting;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", avgRaiting=" + avgRaiting +
                '}';
    }
}
