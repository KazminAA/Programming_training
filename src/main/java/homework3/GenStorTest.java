package homework3;

import java.util.Arrays;

/**
 * Created by Alex on 10.08.2016.
 */
public class GenStorTest {
    public static void main(String[] args) {
        GenericStorage<Integer> gs = new GenericStorage<>();
        System.out.println("size = " + gs.getSize() + " and capasity = " + gs.getCapasity());
        gs.add (25);
        System.out.println("size = " + gs.getSize() + " and capasity = " + gs.getCapasity());
        gs.add (36);
        System.out.println("size = " + gs.getSize() + " and capasity = " + gs.getCapasity());
        gs.add (12);
        System.out.println("size = " + gs.getSize() + " and capasity = " + gs.getCapasity());
        gs.add (5);
        System.out.println("size = " + gs.getSize() + " and capasity = " + gs.getCapasity());
        gs.add (21);
        System.out.println("size = " + gs.getSize() + " and capasity = " + gs.getCapasity());
        gs.add (18);
        System.out.println("size = " + gs.getSize() + " and capasity = " + gs.getCapasity());
        gs.add (137);
        System.out.println("size = " + gs.getSize() + " and capasity = " + gs.getCapasity());
        gs.add (64);
        System.out.println("size = " + gs.getSize() + " and capasity = " + gs.getCapasity());
        gs.add (18);
        System.out.println("size = " + gs.getSize() + " and capasity = " + gs.getCapasity());
        gs.add (11);
        System.out.println("size = " + gs.getSize() + " and capasity = " + gs.getCapasity());
        gs.add (22);
        System.out.println("size = " + gs.getSize() + " and capasity = " + gs.getCapasity());
        gs.add (18);
        System.out.println(gs);
        System.out.println(gs.get(9));
        Integer[] intArray = new Integer[0];
        intArray = gs.getAll(intArray);
        System.out.println(Arrays.toString(intArray));
        gs.update(9, 105);
        System.out.println(gs.get(9));
        gs.delete(9);
        System.out.println(gs);
        gs.delete(Integer.valueOf(18));
        System.out.println(gs);
    }
}
