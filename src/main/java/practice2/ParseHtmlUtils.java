package practice2;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by lex on 05.08.16.
 */
public class ParseHtmlUtils {
    public static String readStringFromFile(String fileName, String codePage) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), codePage));
        String s = null;
        StringBuilder sb = new StringBuilder();
        while ((s = br.readLine()) != null) {
            sb.append(s).append(System.getProperty("line.separator"));
        }
        s = sb.toString();
        return s;
    }

    public static ArrayList<NameRating> babyNameRatingParser(String fileName, String codePage) throws IOException {
        String toParse = readStringFromFile(fileName, codePage);
        if (toParse == null) {
            return null;
        }
        ArrayList<NameRating> ratingsList = new ArrayList<>();
        NameRating nameRating = null;
        Pattern p = Pattern.compile("<td>(\\d+)</td>[\\s\\S]+?<td>(\\w+)</td>[\\s\\S]+?<td>(\\w+)</td>");
        Matcher m = p.matcher(toParse);
        while (m.find()) {
            nameRating = new NameRating(Integer.valueOf(m.group(1)), m.group(2), m.group(3));
            ratingsList.add(nameRating);
        }
        return ratingsList;
    }

    public static ArrayList<Notebook> notebooksPriceParser(String fileName, String codePage) throws IOException {
        String toParse = readStringFromFile(fileName, codePage);
        if (toParse == null) {
            return null;
        }
        ArrayList<Notebook> notebooksList = new ArrayList<>();
        Notebook notebook = null;
        Pattern p = Pattern.compile("class=\"item_name\"[\\S\\s]+?" +
                "title=\"[\\S\\s]+?\\s([\\S\\s]+?)\\s([\\S\\s]+?)\">[\\S\\s]+?" +
                "class=\"description\">([\\S\\s]+?)<[\\S\\s]+?" +
                "class=\"price cost\">(\\d+)");
        Matcher m = p.matcher(toParse);
        while (m.find()) {
            notebook = new Notebook(m.group(1), m.group(2), m.group(3), Integer.valueOf(m.group(4)));
            notebooksList.add(notebook);
        }
        return notebooksList;
    }
}
