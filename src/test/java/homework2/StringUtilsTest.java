package homework2;

import org.junit.Assert;
import org.junit.Test;

import static homework2.StringUtils.*;

/**
 * Created by lex on 21.07.16.
 */
public class StringUtilsTest {
    @Test
    public void stringTurnerTest() throws Exception {
        String expected = "!dlrow olleH";
        String actual = stringTurner("Hello world!");
        Assert.assertEquals("stringTurnerTest()", expected, actual);
    }

    @Test
    public void isPalendromeTest() throws Exception {
        Assert.assertTrue(isPalendrome("A roza ypala na lapy Azora"));
        Assert.assertFalse(isPalendrome("Kykareky"));
    }

    @Test
    public void lenghthLikerTest() throws Exception {
        String expected = "A roza";
        String actual = lenghthLiker("A roza ypala na lapy Azora");
        Assert.assertEquals("lenghthLinkerTest", expected, actual);
        expected = "Gooooooooooo";
        actual = lenghthLiker("Go");
        Assert.assertEquals("lenghthLinkerTest", expected, actual);
    }

    @Test
    public void changeWordsInStringTest() throws Exception {
        String expected = "edem edem, edem, My.";
        String actual = changeWordsInString("My edem, edem, edem.");
        Assert.assertEquals("changeWordInStringTest", expected, actual);
    }

    @Test
    public void changeWordsInSentenceTest() throws Exception {
        String expected = "edem edem, edem, My. pereedem vseh my I. yspeh pereedem vseh, i bydet nash My.";
        String actual = changeWordsInSentence("My edem, edem, edem. I vseh my pereedem. My pereedem vseh, i bydet nash yspeh.");
        Assert.assertEquals("changeWordInSentenceTest", expected, actual);
    }

    @Test
    public void charactersCheckTest() throws Exception {
        Assert.assertTrue(charactersCheck("abccacbacc"));
        Assert.assertFalse(charactersCheck("abcccdfb"));
    }

    @Test
    public void isDateTest() throws Exception {
        Assert.assertTrue(isDate("22.16.2016"));
        Assert.assertFalse(isDate("22.162016"));
    }

    @Test
    public void isMailTest() throws Exception {
        Assert.assertTrue(isMail("llexxx1980@mail.ua"));
        Assert.assertFalse(isMail("sdhfsjdhfg"));
        Assert.assertTrue(isMail("llexxx_1980@mail.com.ua"));

    }

    @Test
    public void phoneCatcherTest() throws Exception {
        String[] sArr = phoneCatcher("+7(068)605-55-20; +38(098)268-66-77; +38(066)596-25-66");
        String expected = "+38(098)268-66-77";
        String actual = sArr[1];
        Assert.assertEquals("phoneCatcherTest", expected, actual);
    }

}