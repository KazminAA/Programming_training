package practice2;

/**
 * Created by lex on 05.08.16.
 */
public class NameRating {
    private final int rating;
    private final String maleName;
    private final String femaleName;

    public NameRating(int rating, String maleName, String femaleName) {
        this.rating = rating;
        this.maleName = maleName;
        this.femaleName = femaleName;
    }

    public int getRating() {
        return rating;
    }

    public String getMaleName() {
        return maleName;
    }

    public String getFemaleName() {
        return femaleName;
    }

    @Override
    public String toString() {
        return "{" + rating + ", " + maleName + ", " + femaleName + "}";
    }
}
