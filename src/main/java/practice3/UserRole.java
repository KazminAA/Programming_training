package practice3;

import java.io.Serializable;

/**
 * Created by Alex on 16.08.2016.
 */
public enum UserRole implements Serializable{
    USER,
    ADMIN
}
