package practice7.mappers.api;

import practice7.models.Entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by lex on 19.09.16.
 */
public interface SessionMappeble {
    int getId();

    void setId(int id);

    Entity getFilmAsEntity();

    void setFilmAsEntity(Entity film);

    LocalDateTime getDateOfSeance();

    void setDateOfSeance(LocalDateTime dateOfSeance);

    Entity getHallAsEntity();

    void setHallAsEntity(Entity hall);

    BigDecimal getPrice();

    void setPrice(BigDecimal price);
}
