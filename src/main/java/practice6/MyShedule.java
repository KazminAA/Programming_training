package practice6;

import java.util.Map;

/**
 * Created by lex on 06.09.16.
 */
public class MyShedule {
    public static ThreadGroup runMesseging(Map<Message, Long> messageMap) {
        ThreadGroup gr = new ThreadGroup("Messengers");
        for (Map.Entry<Message, Long> entry : messageMap.entrySet()) {
            Thread th = new Thread(gr, () -> {
                while (true) {
                    System.out.println(entry.getKey().getMess());
                    try {
                        Thread.sleep(entry.getValue());
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            });
            th.start();
        }
        return gr;
    }

    public static class Message {
        private static int idGen = 0;
        private String mess;
        private int id;

        public Message(String mess) {
            setMess(mess);
            this.id = idGen;
            idGen++;
        }

        public int getId() {
            return id;
        }

        public static int getIdGen() {
            return idGen;
        }

        public String getMess() {
            return mess;
        }

        public void setMess(String mess) {
            this.mess = mess;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Message)) return false;

            Message message = (Message) o;

            if (getId() != message.getId()) return false;
            return getMess().equals(message.getMess());

        }

        @Override
        public int hashCode() {
            int result = getMess().hashCode();
            result = 31 * result + getId();
            return result;
        }
    }
}
