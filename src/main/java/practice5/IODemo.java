package practice5;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alex on 30.08.2016.
 */
public class IODemo {

    private static List<String> readStrFile(String filename) {
        List<String> listFromFile = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "UTF-8"))) {
            String tmpStr;
            while ((tmpStr = br.readLine()) != null) {
                listFromFile.add(tmpStr);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listFromFile;
    }

    private static void writeListToFile(String filename, List<String> list) {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"))) {
            for (String s : list) {
                bw.write(s);
                bw.write('\n');
                bw.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void randomNumberFileGenerator(String filename, int count) {
        Random random = new Random();
        List<String> toFile = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            toFile.add(Integer.toString(random.nextInt(10000)));
        }
        writeListToFile(filename, toFile);
    }

    public static void sortedIntFromRandom(String filename) {
        List<String> intFromFile = readStrFile(filename);
        Collections.sort(intFromFile);
        filename = filename + "_sorted";
        writeListToFile(filename, intFromFile);
    }

    public static void studentsAvgAbove(int raiting) {
        List<String> students = readStrFile("StudentAvgRaiting.txt");
        Map<String, List<Integer>> stRaitigs = new HashMap<>();
        Pattern p = Pattern.compile("(.+)\\s=\\s(\\d+)");
        Matcher m;
        List<Integer> tmpList;
        for (String student : students) {
            m = p.matcher(student);
            if (!m.find()) break;
            if ((tmpList = stRaitigs.get(m.group(1))) != null) {
                tmpList.add(Integer.parseInt(m.group(2)));
            } else {
                tmpList = new ArrayList<>();
                tmpList.add(Integer.parseInt(m.group(2)));
                stRaitigs.put(m.group(1), tmpList);
            }
        }
        for (Map.Entry<String, List<Integer>> listEntry : stRaitigs.entrySet()) {
            tmpList = listEntry.getValue();
            int sum = 0;
            for (Integer integer : tmpList) {
                sum += integer;
            }
            if ((sum = sum / tmpList.size()) > raiting) {
                System.out.println(listEntry.getKey() + " avg: " + sum);
            }
        }
    }

    private static List<String> changeWord(String filename, String pattern) {
        List<String> tmpList = readStrFile(filename);
        Pattern p = Pattern.compile(pattern);
        Matcher m;
        String tmpStr = null;
        for (int i = 0; i < tmpList.size(); i++) {
            m = p.matcher(tmpList.get(i));
            if (m.find()) {
                tmpStr = m.replaceAll("$1$4$3$2$5");
            }
            tmpList.set(i, tmpStr);
        }
        return tmpList;
    }

    public static void changeWordString(String filename) {
        List<String> tmpList = changeWord(filename, "^(\\s*)([\\s\\S]+?)(\\s+[\\S\\s]+?)([^\\s]+?)([.!?]*?)$");
        tmpList.forEach(System.out::println);
    }

    public static void changeWordSantence(String filename) {
        List<String> tmpList = changeWord(filename, "(\\s*)([\\s\\S]+?)(\\s+[\\S\\s]+?)([^\\s]+?)([.!?]+?)");
        tmpList.forEach(System.out::println);
    }

    public static void copyMetter() throws IOException {
        FileReader fr = new FileReader("Romeo.txt");
        FileWriter fw = new FileWriter("Romeo_copy.txt");
        int ch;
        Long time = System.currentTimeMillis();
        while ((ch = fr.read()) != -1) {
            fw.write(ch);
        }
        time = System.currentTimeMillis() - time;
        if (fr != null) {
            fr.close();
        }
        if (fw != null) {
            fw.close();
        }
        System.out.println("UFRTime: " + time + "ms");
        BufferedReader br = new BufferedReader(new FileReader("Romeo.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("Romeo_copy.txt"));
        String tmpStr;
        time = System.currentTimeMillis();
        while ((tmpStr = br.readLine()) != null) {
            bw.write(tmpStr);
        }
        time = System.currentTimeMillis() - time;
        if (br != null) {
            br.close();
        }
        if (bw != null) {
            bw.close();
        }
        System.out.println("BFRTime: " + time + "ms");
        FileInputStream fis = new FileInputStream("Romeo.txt");
        FileOutputStream fos = new FileOutputStream("Romeo_copy.txt");
        time = System.currentTimeMillis();
        while ((ch = fis.read()) != -1) {
            fos.write(ch);
        }
        time = System.currentTimeMillis() - time;
        if (fis != null) {
            br.close();
        }
        if (fos != null) {
            bw.close();
        }
        System.out.println("USRTime: " + time + "ms");
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("Romeo.txt"));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("Romeo_copy.txt"));
        time = System.currentTimeMillis();
        while ((ch = bis.read()) != -1) {
            bos.write(ch);
        }
        time = System.currentTimeMillis() - time;
        if (bis != null) {
            br.close();
        }
        if (bos != null) {
            bw.close();
        }
        System.out.println("BSRTime: " + time + "ms");
    }

    public static void groupWrite(String filename, Group group) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
            oos.writeObject(group);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Group groupRead(String filename) {
        Group group = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
            group = (Group) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return group;
    }
}
