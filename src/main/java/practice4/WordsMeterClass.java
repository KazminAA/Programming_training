package practice4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alex on 22.08.2016.
 */
public class WordsMeterClass {
    public static Map<String, Integer> wordsMeter(String filename) throws IOException {
        Map<String, Integer> wordsMap = new HashMap<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
        String allText = null;
        StringBuilder sb = new StringBuilder();
        while ((allText = br.readLine()) != null) {
            sb.append(allText);
        }
        allText = sb.toString();
        Pattern pattern = Pattern.compile("\\w+");
        Matcher matcher = pattern.matcher(allText);
        String word = null;
        Integer value = null;
        while (matcher.find()) {
            word = matcher.group();
            if ((value = wordsMap.get(word)) != null) {
                value = Integer.valueOf(value.intValue() + 1);
            } else {
                value = 1;
            }
            wordsMap.put(word, value);
        }
        return wordsMap;
    }

    public static Map<String, Integer> wordsMeter(String filename, sortMethod sortMethod) throws IOException {
        Map<String, Integer> wordsMap = wordsMeter(filename);
        List<Map.Entry<String, Integer>> entryList = new ArrayList<>(wordsMap.entrySet());
        switch (sortMethod) {
            case WORD_LH: {
                entryList.sort((o1, o2) -> o1.getKey().compareTo(o2.getKey()));
                break;
            }
            case WORD_HL: {
                entryList.sort((o1, o2) -> o2.getKey().compareTo(o1.getKey()));
                break;
            }
            case COUNT_HL: {
                entryList.sort((o1, o2) -> o2.getValue() - o1.getValue());
                break;
            }
            case COUNT_LH: {
                entryList.sort((o1, o2) -> o1.getValue() - o2.getValue());
                break;
            }
        }
        wordsMap = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> sIEntry : entryList) {
            wordsMap.put(sIEntry.getKey(), sIEntry.getValue());
        }
        return wordsMap;
    }
}
