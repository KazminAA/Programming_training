package practice3;

import java.io.Serializable;

/**
 * Created by Alex on 16.08.2016.
 */
public abstract class Entity implements Serializable {
    long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
