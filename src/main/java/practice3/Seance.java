package practice3;

import java.time.LocalDateTime;

/**
 * Created by Alex on 17.08.2016.
 */
public class Seance extends Entity {
    private LocalDateTime dateOfSeance;
    //private LocalTime timeOfSeance;
    Film film;

    public LocalDateTime getDateOfSeance() {
        return dateOfSeance;
    }

    public void setDateOfSeance(LocalDateTime dateOfSeance) {
        this.dateOfSeance = dateOfSeance;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }
}
