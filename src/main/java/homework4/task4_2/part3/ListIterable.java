package homework4.task4_2.part3;

/**
 * Created by Alex on 19.08.2016.
 */
public interface ListIterable<E> {
    ListIterator<E> listIterator();
}
