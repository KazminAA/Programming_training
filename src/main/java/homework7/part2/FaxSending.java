package homework7.part2;

/**
 * Created by Alexandr on 06.09.2016.
 */
public class FaxSending implements Runnable {
    private Office from;
    private Office to;

    public FaxSending(Office from, Office to) {
        setFrom(from);
        setTo(to);
    }

    public Office getFrom() {
        return from;
    }

    public void setFrom(Office from) {
        this.from = from;
    }

    public Office getTo() {
        return to;
    }

    public void setTo(Office to) {
        this.to = to;
    }

    @Override
    public void run() {
        this.from.getFax().sendFax(this.from, this.to);
    }
}
