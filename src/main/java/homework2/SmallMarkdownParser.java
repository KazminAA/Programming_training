package homework2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by lex on 28.07.16.
 */
public class SmallMarkdownParser {

    public static String[] readFromFile(String filePath) throws IOException {
        ArrayList<String> textToParse = new ArrayList<>();
        String tmpStr = "";
        String[] toParse = null;
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            while ((tmpStr = br.readLine()) != null) {
                textToParse.add(tmpStr);
            }
        } catch (FileNotFoundException e) {
            System.out.println("404: FileNotFound");
        }
        if (textToParse.size() != 0) {
            toParse = new String[textToParse.size()];
            textToParse.toArray(toParse);
        }
        return toParse;
    }

    public static String[] readFomString (String strToParse) {
        String[] textToParse = strToParse.split("\n");
        textToParse = parseText(textToParse);
        return textToParse;
    }

    public static String[] parseText(String[] textToParse) {
        String[] parsedText = null;
        StringBuilder sb;
        String tmpStr = "";
        Pattern p = null;
        Matcher m = null;
        if (textToParse != null) {
            int strToParseCount = textToParse.length;
            parsedText = new String[strToParseCount + 4];
            parsedText[0] = "<html>";
            parsedText[1] = "<body>";
            parsedText[strToParseCount + 2] = "</body>";
            parsedText[strToParseCount + 3] = "</html>";
            for (int i = 0; i < strToParseCount; i++) {
                sb = new StringBuilder();
                tmpStr = textToParse[i];
                p = Pattern.compile("(^#+)(.+)");
                m = p.matcher(tmpStr);
                if (m.find()) { //if line is header parse it like header
                    int index = 0;
                    while (tmpStr.indexOf("#", index) > -1) {
                        index++;
                    }
                    String s = "h" + index + ">";
                    sb.append(m.replaceAll("<" + s + "$2" + "</" + s));
                } else { //parse line as simple line
                    sb.append("<p>");
                    p = Pattern.compile("\\[(.*)\\]\\((.*)\\)"); //replase all links
                    m = p.matcher(tmpStr);
                    tmpStr = m.replaceAll("<a href=\"$2\">$1</a>");
                    p = Pattern.compile("\\*\\*([^\\*]+)\\*\\*");
                    m = p.matcher(tmpStr);
                    tmpStr = m.replaceAll("<strong>$1</strong>"); //parse all strong
                    p = Pattern.compile("\\*(.+?)\\*"); //parse all am
                    m = p.matcher(tmpStr);
                    tmpStr = m.replaceAll("<em>$1</em>");
                    sb.append(tmpStr);
                    sb.append("</p>");
                }
                parsedText[i + 2] = sb.toString();
            }
        }
        return parsedText;
    }
}
