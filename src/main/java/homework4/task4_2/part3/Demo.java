package homework4.task4_2.part3;


/**
 * Created by Alex on 19.08.2016.
 */
public class Demo {
    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addFirst(8.8);
        deque.addLast(10);
        deque.addFirst(11);
        deque.addLast(5.12);
        deque.addLast(3);

        System.out.println(deque);

        ListIterator<Number> deqIterator = deque.listIterator();

        /*while (deqIterator.hasNext()) {
            if (deqIterator.next().equals(3)) {
                deqIterator.remove();}
        }

        System.out.println(deque);*/

        while (deqIterator.hasNext()) {
            System.out.println(deqIterator.next());
        }

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        deqIterator.newUse();

        while (deqIterator.hasPrevious()) {
            Number num = deqIterator.previous();
            System.out.println(num);
            if (num.equals(10)) {
                deqIterator.set(22);
            }
        }

        System.out.println(deque);
    }
}
