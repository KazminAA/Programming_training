package homework4.task4_2.part2;

import java.util.Iterator;

/**
 * Created by Alex on 19.08.2016.
 */
public class Demo {
    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addFirst(8.8);
        deque.addLast(10);
        deque.addFirst(11);
        deque.addLast(5.12);
        deque.addLast(3);

        System.out.println(deque);

        for (Number number : deque) {
            System.out.println(number);
        }

        Iterator<Number> deqIterator = deque.iterator();

        while (deqIterator.hasNext()) {
            if (deqIterator.next().equals(8.8)) {
                deqIterator.remove();
            }
        }

        System.out.println(deque);

    }
}
