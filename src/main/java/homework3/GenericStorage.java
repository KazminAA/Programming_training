package homework3;

import java.util.Arrays;

/**
 * Created by Alex on 10.08.2016.
 */
public class GenericStorage<T> {
    private Object[] storage;
    private int size;

    public GenericStorage() {
        setStorage(new Object[10]);
    }

    public GenericStorage(int capasity) {
        setStorage(new Object[capasity]);
    }

    public int getCapasity () {
        return storage.length;
    }

    private void setStorage(Object[] storage) {
        this.storage = storage;
    }

    public int getSize() {
        return size;
    }

    private void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return Arrays.toString(this.storage);
    }

    private boolean checkIndex(int index) throws IndexOutOfBoundsException {
        if ((index > getCapasity() - 1) || (index < 0)) throw new IndexOutOfBoundsException(
                "index must be <= " + (getCapasity() - 1));
        return true;
    }

    /**
     * add an object to end of storage. if storage is full than size of capasity of storage will be
     * increased by 10.
     *
     * @param obj
     */

    public void add (T obj) {
        int size = getSize();
        if (size < getCapasity()) {
        }
        else {
            Object[] tempArr = new Object[getCapasity() + 10];
            System.arraycopy(this.storage, 0, tempArr, 0, getCapasity());
            setStorage(tempArr);
        }
        this.storage[size] = obj;
        setSize(++size);
    }

    /**
     * get an object
     *
     * @param index idex of object to deleteC
     * @return an object
     * @throws IndexOutOfBoundsException
     */

    public T get(int index) throws IndexOutOfBoundsException {
        checkIndex(index);
        return (T) this.storage[index];
    }

    /**
     * Returns an array containing all of the elements in this storage
     *
     * @return an array of objects in store
     */

    public T[] getAll(T[] clazz) {
//        T[] newArr = (T[]) new Object[getSize()];
//        for (int i = 0; i < getSize(); i++) {
//            newArr[i] = (T) storage[i];
//        }
//        System.out.println(newArr.getClass().getName());
//        return newArr;
        return (T[]) Arrays.copyOf(storage, size, clazz.getClass());
    }

    /**
     * update an object in storage
     *
     * @param index the index of element than updating
     * @param obj   the object that raplace an element in storage
     * @throws IndexOutOfBoundsException
     */

    public void update(int index, T obj) throws IndexOutOfBoundsException {
        checkIndex(index);
        this.storage[index] = obj;
    }

    /**
     * deleteC the object in index
     *
     * @param index of object to deleteC
     * @throws IndexOutOfBoundsException
     */

    public void delete(int index) throws IndexOutOfBoundsException {
        checkIndex(index);
        int size = getSize();
        for (int i = index; i < size; i++) {
            this.storage[i] = this.storage[i + 1];
        }
        setSize(--size);
    }

    /**
     * deleteC object from storage. if there is some objects will be deleteC all
     *
     * @param obj an object to deleteC
     */

    public void delete(T obj) {
        for (int i = 0; i < getSize(); ) {
            if (this.storage[i].equals(obj)) {
                delete(i);
            } else i++;
        }
    }
}
