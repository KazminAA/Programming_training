package practice1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by lex on 12.07.16.
 */
public class StudentTest {
    public Student student;

    @Before
    public void initStudent() {
        student = new Student("Vasya", "Pypkin", 4, "Data protection");
        student.addExamToList("Mathematics", 4, 1997, 1);
        student.addExamToList("Mathematics", 3, 1997, 2);
        student.addExamToList("Physics", 3, 1997, 1);
        student.addExamToList("C# programming", 3, 1997, 1);
        student.addExamToList("Special sections of mathematics", 3, 1998, 1);
    }

    @Test
    public void maxRaitingOfExam() {
        System.out.println("");
        System.out.println("~~~~~~~~~~~~~~~Test for Max Rating~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("~~~~~~~~~~~init student~~~~~~~~~~~~~~");
        System.out.println(student);
        student.printExamsConsole();
        int expected = 4;
        int actual = student.maxRaitingOfExam("Mathematics");
        Assert.assertEquals("Test for maxRiting", expected, actual);
    }

    @Test
    public void addExamToList() {
        System.out.println("");
        System.out.println("~~~~~~~~~~~~~~~~addExamToList() test~~~~~~~~~~~~~~~~~~");
        student.printExamsConsole();
        System.out.println("After-------------------------------------");
        student.addExamToList("Test", 5, 1997, 1);
        Student.Exam ex1 = new Student.Exam("Test", 5, 1997, 1);
        boolean flag = false;
        for (Student.Exam ex : student.getExams()) {
            if (ex.equals(ex1)) {
                flag = true;
            }
        }
        student.printExamsConsole();
        Assert.assertTrue(flag);
    }

    @Test
    public void removeExamFromList() throws ExamDoesNotExist {
        System.out.println("");
        System.out.println("~~~~~~~~~~removeExamFromList() test~~~~~~~~~~~~~~");
        student.printExamsConsole();
        System.out.println("After-----------------------------------");
        boolean flag = false;
        student.removeExamFromList("Special sections of mathematics", 3, 1998, 1);
        Student.Exam ex1 = new Student.Exam("Special sections of mathematics", 3, 1998, 1);
        for (Student.Exam ex : student.getExams()) {
            if (ex.equals(ex1)) {
                flag = true;
            }
        }
        student.printExamsConsole();
        Assert.assertFalse(flag);
    }

    @Test(expected = ExamDoesNotExist.class)
    public void removeExamFromListException() throws ExamDoesNotExist {
        student.removeExamFromList("Special sections", 3, 2005, 1);
    }

    @Test
    public void calculateAmountOfExamWithRating() throws Exception {
        int expected = 4;
        int actual = student.calculateAmountOfExamWithRating(3);
        Assert.assertEquals("Test for AmountOfExamWithRating", expected, actual);
    }

    @Test
    public void avgRatingOfSemester() throws Exception {
        System.out.println("");
        System.out.println("~~~~~~~~~~~~~~~~~~~~Test avg~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        student.printExamsConsole();
        double expected = 3.33;
        double actual = student.avgRatingOfSemester(1997, 1);
        System.out.println(actual);
        Assert.assertEquals("Test fo AvgratingInSemester", expected, actual, 0.01);
    }

}