package homework7;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Alexandr on 05.09.2016.
 */
public class TimePrint extends Thread {
    @Override
    public void run() {
        while (true) {
            System.out.println(LocalTime.now().format(DateTimeFormatter.ofPattern("HH':'mm':'ss")) + " " + Thread.currentThread().getName());
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
