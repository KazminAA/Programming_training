package practice5;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexandr on 31.08.2016.
 */
public class Group implements Serializable {
    private List<Student> group = new ArrayList<>();

    public Group(Student... students) {
        for (Student student : students) {
            group.add(student);
        }
    }

    public List<Student> getGroup() {
        return group;
    }

    public void setGroup(List<Student> group) {
        this.group = group;
    }
}
