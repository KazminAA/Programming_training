package practice4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Alex on 22.08.2016.
 */
public class StUtilsDemo {
    public static void main(String[] args) {
        List<Student> stList = new ArrayList<>();
        stList.add(new Student("Denis", "Petrov", 1));
        stList.add(new Student("Alexandr", "Petrov", 2));
        stList.add(new Student("Denis", "Petrov", 3));
        stList.add(new Student("Stas", "Sidorov", 4));
        stList.add(new Student("Anna", "Egorova", 1));
        stList.add(new Student("Masha", "Soveleva", 2));
        
        Map<String, Student> stMap = StudentUtils.createMapFromList(stList);

        for (Map.Entry<String, Student> entry: stMap.entrySet()) {
            System.out.println(entry.getKey() + "->" + entry.getValue());
        }

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        StudentUtils.studentPrint(stList, 2);

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        stList = StudentUtils.studentSort(stList);
        stList.forEach(System.out::println);
    }
}
