package practice1;

/**
 * Created by lex on 12.07.16.
 */
public class ArraySum {
    private double[] arrForSum;

    public ArraySum() {
    }

    public ArraySum(double[] dArray) {
        this.setArrForSum(dArray);
    }

    public double[] getArrForSum() {
        return arrForSum;
    }

    public void setArrForSum(double[] arrForSum) {
        this.arrForSum = arrForSum;
    }

    public static double sum(double[] dArray) {
        double sum = 0;
        for (double d : dArray) {
            sum += d;
        }
        return sum;
    }

    public double sum() {
        double sum = 0;
        if (this.getArrForSum() != null) {
            for (double d : this.getArrForSum()) {
                sum += d;
            }
        }
        return sum;
    }
}
