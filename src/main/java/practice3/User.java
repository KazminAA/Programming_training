package practice3;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 16.08.2016.
 */
public class User extends Entity {
    private String login;
    private String email;
    private String userName;
    private String userSurname;
    private String userInfo;
    private UserRole role;
    private List<Ticket> userTickets = new ArrayList<>();

    public User(String login, String email, String userName, String userSurname, String userInfo, UserRole role) {
        setLogin(login);
        setEmail(email);
        setUserName(userName);
        setUserSurname(userSurname);
        setUserInfo(userInfo);
        setRole(role);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    public String getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(String userInfo) {
        this.userInfo = userInfo;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public List<Ticket> getUserTickets() {
        return userTickets;
    }

    public void setUserTickets(List<Ticket> userTickets) {
        this.userTickets = userTickets;
    }

    @Override
    public String toString() {
        return "{" +
                "login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", '" + userName + userSurname + '\'' +
                '}';
    }
}
