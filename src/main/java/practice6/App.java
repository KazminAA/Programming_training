package practice6;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lex on 06.09.16.
 */
public class App {
    public static void main(String[] args) {
        printRunnable();
        printThread();
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Map<MyShedule.Message, Long> messageMap = new HashMap<>();
        messageMap.put(new MyShedule.Message("Voooo!!!"), (long) 12);
        messageMap.put(new MyShedule.Message("Ooops!!!"), (long) 5);
        messageMap.put(new MyShedule.Message("Booom!!!"), (long) 3);
        messageMap.put(new MyShedule.Message("Oh my god!"), (long) 8);
        messageMap.put(new MyShedule.Message("Yahooo!!!"), (long) 5);
        ThreadGroup gr = MyShedule.runMesseging(messageMap);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        gr.interrupt();
    }

    public static void printThread() {
        Thread th = new Thread() {
            @Override
            public void run() {
                long time = System.currentTimeMillis();
                for (int i = 0; i < 10; i++) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName());
                }
                time = time - System.currentTimeMillis();
                System.out.println(Thread.currentThread().getName() + " end in " + time + "ms.");
            }
        };
        th.start();
    }

    public static void printRunnable() {
        Thread th = new Thread(() -> {
            long time = System.currentTimeMillis();
            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
            }
            time = time - System.currentTimeMillis();
            System.out.println(Thread.currentThread().getName() + " end in " + time + "ms.");
        }, "Runnable-0");
        th.start();
    }
}
