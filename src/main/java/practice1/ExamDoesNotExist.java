package practice1;

/**
 * Created by lex on 17.07.16.
 */
public class ExamDoesNotExist extends Exception {
    public ExamDoesNotExist(String message) {
        super(message);
    }
}
