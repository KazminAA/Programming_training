package practice2;

/**
 * Created by lex on 05.08.16.
 */
public class Notebook {
    private final String vendorName;
    private final String model;
    private final String description;
    private final int price;

    public Notebook(String vendorName, String model, String description, int price) {
        this.vendorName = vendorName;
        this.model = model;
        this.description = description;
        this.price = price;
    }

    public String getVendorName() {
        return vendorName;
    }

    public String getModel() {
        return model;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "{" + vendorName + ", " + model +
                ", \"" + description.substring(0, 30) + "...\"" +
                ", " + price +
                '}';
    }
}
