package practice3;

/**
 * Created by Alex on 17.08.2016.
 */
public class InMemoryDB {
    public static Container<Film> films = new Container<>(1);
    public static Container<User> users = new Container<>(1);
    //private Container<UserRole> userroles = new Container<>(1);
    public static Container<Hall> halls = new Container<>(1);
    public static Container<Ticket> tickets = new Container<>(1);
    public static Container<Cinema> cinemas = new Container<>(1);
}
