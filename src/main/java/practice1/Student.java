package practice1;

import java.util.ArrayList;

/**
 * Created by lex on 12.07.16.
 */
public class Student {
    private String name;
    private String surName;
    private Group group;
    private ArrayList<Exam> exams = new ArrayList<>();

    public Student(String name, String surName, int clas, String department) {
        setName(name);
        setSurName(surName);
        setGroup(new Group(clas, department));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void addExamToList(String name, int rating, int year, int semester) {
        Exam ex = new Exam(name, rating, year, semester);
        if (exams.indexOf(ex) == -1) {
            exams.add(ex);
        } else System.out.println("Exam already exist.");
    }

    public ArrayList<Exam> getExams() {
        return exams;
    }

    public void removeExamFromList(String name, int rating, int year, int semester) throws ExamDoesNotExist {
        Exam ex = new Exam(name, rating, year, semester);
        int index = exams.indexOf(ex);
        if (index != -1) {
            exams.remove(index);
        } else throw new ExamDoesNotExist("There is no such exam.");
    }

    public void printExamsConsole() {
        for (Exam ex : exams) {
            System.out.println(ex);
        }
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surName + '\'' +
                ", group=" + group +
                '}';
    }

    public int maxRaitingOfExam(String name) {
        int maxRaiting = -1;
        for (Exam ex : exams) {
            if (ex.getName().equals(name) & (ex.getRating() > maxRaiting)) {
                maxRaiting = ex.getRating();
            }
        }
        return maxRaiting;
    }

    public int calculateAmountOfExamWithRating(int rating) {
        int amount = 0;
        for (Exam e : getExams()) {
            if (e.getRating() == rating) amount++;
        }
        return amount;
    }

    public double avgRatingOfSemester(int year, int semester) {
        double avg = 0;
        int amount = 0;
        for (Exam e : getExams()) {
            if (e.getDate().findDate(year, semester)) {
                avg += e.getRating();
                amount++;
            }
        }
        if (!(amount == 0)) return avg / amount;
        else return 0;
    }

    public class Group {
        int clas;
        String department;

        public Group(int clas, String department) {
            setClas(clas);
            setDepartment(department);
        }

        public int getClas() {
            return clas;
        }

        public void setClas(int clas) {
            this.clas = clas;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        @Override
        public String toString() {
            return "{" +
                    "class '" + clas +
                    "', '" + department + '\'' +
                    '}';
        }
    }

    public static class Exam {
        private String name;
        private int rating;
        private CompletionDate date;

        public Exam(String name, int rating, int year, int semester) {
            setName(name);
            setRating(rating);
            setDate(new CompletionDate(year, semester));
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public CompletionDate getDate() {
            return date;
        }

        public void setDate(CompletionDate date) {
            this.date = date;
        }

        @Override
        public String toString() {
            return "Exam{" +
                    "name='" + name + '\'' +
                    ", rating=" + rating +
                    ", date=" + date +
                    '}';
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (!obj.getClass().equals(this.getClass())) return false;
            return (((Exam) obj).getName().equals(this.getName())) && (((Exam) obj).getRating() == this.getRating()) &&
                    (((Exam) obj).getDate().equals(this.getDate()));
        }

        @Override
        public int hashCode() {
            return getName().hashCode() + getRating() * 1251 + getDate().hashCode();
        }

        public class CompletionDate {
            private int year;
            private int semester;

            public CompletionDate(int year, int semester) {
                setYear(year);
                setSemester(semester);
            }

            public int getYear() {
                return year;
            }

            public void setYear(int year) {
                this.year = year;
            }

            public int getSemester() {
                return semester;
            }

            public void setSemester(int semester) {
                this.semester = semester;
            }

            @Override
            public String toString() {
                return "{" +
                        "year=" + year +
                        ", semester=" + semester +
                        '}';
            }

            @Override
            public boolean equals(Object obj) {
                if (obj == null) return false;
                if (!obj.getClass().equals(this.getClass())) return false;
                return (((CompletionDate) obj).getYear() == this.getYear()) && (((CompletionDate) obj).getSemester() == this.getSemester());
            }

            public boolean findDate(int year, int semester) {
                return (this.getYear() == year) && (this.getSemester() == semester);
            }

            @Override
            public int hashCode() {
                return super.hashCode() + getSemester() * 20 + getYear() * 7;
            }
        }
    }
}
