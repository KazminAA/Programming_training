package homework1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by lex on 11.07.16.
 * class Date implements some function to work with date
 */
public class Date implements Comparable {
    private Year year;
    private Month month;
    private Day day;
    private int daysToZeroPoint; //number of days from 01.01.1582

    /**
     * Constructor
     *
     * @param year  ineger value that represent year
     * @param month ineger value that represent month
     * @param day   ineger value that represent day
     * @throws IOException
     */

    public Date(int year, int month, int day) throws IOException {
        setYear(year);
        setMonthByNumber(month);
        setDay(day);
    }

    public Year getYear() {
        return this.year;
    }

    public void setYear(int year) {
        if (year < 1582) {
            year = 1582;
        }
        this.year = this.new Year(year);
    }

    public Month getMonth() {
        return month;
    }

    /**
     * Method to set month with name
     * @param month String value that most take the values such APRIL, MAY
     * @throws IOException
     */

    public void setMonthByName(String month) throws IOException {
        try {
            this.month = Month.valueOf(month.toUpperCase());
        } catch (IllegalArgumentException e) {
            System.out.println("Wrong month. Try to set manual.");
            setMonthByNumber(20);
        }
    }

    /**
     * Method to set month by number value
     * @param month ineger value that most be between 1 and 12
     * @throws IOException
     */

    public void setMonthByNumber(int month) throws IOException {
        while ((month < 1) || (month > 12)) {
            month = getIntConsole("Wrong month. Enter number from 1 to 12.");
        }
        switch (month) {
            case 1: {
                this.month = Month.JANUARY;
                break;
            }
            case 2: {
                this.month = Month.FEBRUARY;
                break;
            }
            case 3: {
                this.month = Month.MARCH;
                break;
            }
            case 4: {
                this.month = Month.APRIL;
                break;
            }
            case 5: {
                this.month = Month.MAY;
                break;
            }
            case 6: {
                this.month = Month.JUNE;
                break;
            }
            case 7: {
                this.month = Month.JULY;
                break;
            }
            case 8: {
                this.month = Month.AUGUST;
                break;
            }
            case 9: {
                this.month = Month.SEPTEMBER;
                break;
            }
            case 10: {
                this.month = Month.FEBRUARY;
                break;
            }
            case 11: {
                this.month = Month.NOVEMBER;
                break;
            }
            case 12: {
                this.month = Month.DECEMBER;
                break;
            }
        }
        if (getDay() != null) {
            setDay(getDay().getNumber());
        }
    }

    public Day getDay() {
        return day;
    }

    public void setDay(int day) throws IOException {
        this.day = new Day(day);
        setDaysToZeroPoint(calculateDaysToZiroPoint());
    }

    public void setDaysToZeroPoint(int daysToZeroPoint) {
        this.daysToZeroPoint = daysToZeroPoint;
    }

    public int getDaysToZeroPoint() {
        return daysToZeroPoint;
    }

    @Override
    public int compareTo(Object o) {
        if ((o == null) || !this.getClass().getName().equals(o.getClass().getName())) {
            throw new IllegalArgumentException();
        }
        return this.getDaysToZeroPoint() - ((Date) o).getDaysToZeroPoint();
    }

    @Override
    public String toString() {
        return "Date{" +
                "day=" + day + "(" + getDay().getDayOfWeek() + ")" +
                ", month=" + month +
                ", year=" + year +
                '}';
    }

    /**
     * class Year represent year in class Date.
     */

    public class Year {
        private int yearNumber;
        private boolean leap = false;

        public Year(int yearNumber) {
            setYearNumber(yearNumber);
        }

        public int getYearNumber() {
            return yearNumber;
        }

        /**
         * set number of year then check the year is leap, if true set the number of days in february to 29
         *
         * @param yearNumber integer value of year
         */

        public void setYearNumber(int yearNumber) {
            this.yearNumber = yearNumber;
            if (yearNumber % 4 == 0 && yearNumber % 100 != 0 || yearNumber % 400 == 0) {
                setLeap(true);
                Month.FEBRUARY.setDaysNumber(29);
            } else {
                setLeap(false);
                Month.FEBRUARY.setDaysNumber(28);
            }
        }

        public boolean isLeap() {
            return leap;
        }

        private void setLeap(boolean leap) {
            this.leap = leap;
        }

        @Override
        public String toString() {
            return Integer.toString(getYearNumber());
        }
    }

    /**
     * enumeration Month represent month in class Date
     */

    public static enum Month {
        JANUARY(1, 31),
        FEBRUARY(2, 28),
        MARCH(3, 31),
        APRIL(4, 30),
        MAY(5, 31),
        JUNE(6, 30),
        JULY(7, 31),
        AUGUST(8, 31),
        SEPTEMBER(9, 30),
        OCTOBER(10, 31),
        NOVEMBER(11, 30),
        DECEMBER(12, 31);

        private int daysNumber;
        private final int monthOfYear;

        Month(int monthOfYear, int daysNumber) {
            this.monthOfYear = monthOfYear;
            setDaysNumber(daysNumber);
        }

        public int getDaysNumber() {
            return daysNumber;
        }

        private void setDaysNumber(int daysNumber) {
            this.daysNumber = daysNumber;
        }

        public int getMonthOfYear() {
            return monthOfYear;
        }
    }

    /**
     * class Day represent day in class Date.
     */

    public class Day {
        private String dayOfWeek; //day of week
        private int number; //number of day
        private int dayOfYear; //day of year

        private Day(int day) throws IOException {
            setNumber(day);
        }

        public String getDayOfWeek() {
            return dayOfWeek;
        }

        private void setDayOfWeek(String dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
        }

        /**
         * Method to calculate and set day of year, that meen how many days have passed from 01.01
         * @param number integer value of day
         */

        private void setDayOfYear(int number) {
            int days = 0;
            if (getMonth() != null) {
                Month[] m = Month.values();
                for (int i = 0; i < getMonth().getMonthOfYear() - 1; i++) {
                    days += m[i].getDaysNumber();
                }
                days += number;
            }
            this.dayOfYear = days;
        }

        public int getDayOfYear() {
            return dayOfYear;
        }

        public int getNumber() {
            return number;
        }

        /**
         * Method to calculate day of week
         * @param number ineger value of day
         * @return string value of day of week
         */

        private String calculateDateOfWeek(int number) {
            String[] day = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
            int a = (14 - getMonth().getMonthOfYear()) / 12;
            int y = getYear().getYearNumber() - a;
            int m = getMonth().getMonthOfYear() + 12 * a - 2;
            return day[(7000 + (number + y + y / 4 - y / 100 + y / 400 + (31 * m) / 12)) % 7];
        }

        public void setNumber(int number) throws IOException {
            while ((number < 1) || (number > getMonth().getDaysNumber())) {
                number = getIntConsole("Wrong day must be > 1 adn < " + getMonth().getDaysNumber() + ".");
            }
            this.number = number;
            setDayOfWeek(calculateDateOfWeek(number));
            setDayOfYear(number);
        }

        @Override
        public String toString() {
            String s = "";
            if (getNumber() < 10) {
                s += "0" + getNumber();
            } else {
                s = Integer.toString(getNumber());
            }
            return s;
        }
    }

    /**
     * calculate number of days from 01.01.1582 to date
     *
     * @return integer value of days
     */

    private int calculateDaysToZiroPoint() {
        int currentYear = getYear().getYearNumber();
        int yearsBetween = 0;
        if (currentYear > 1582) {
            yearsBetween = currentYear - 1582 + 1;
        }
        int delta = 0;
        for (int i = 0; i < yearsBetween; i++) {
            if (currentYear % 4 == 0 && currentYear % 100 != 0 || currentYear % 400 == 0) {
                delta++;
            }
            currentYear--;
        }
        return delta + yearsBetween * 365 + (getDay().getDayOfYear() - 1);
    }

    /**
     * Method to calculate value of days between two date
     * @param date object of Date class
     * @return integer value of days
     */

    public int daysBetween(Date date) {
        int db = compareTo(date);
        if (db < 0) {
            db = db * (-1);
        }
        return db;
    }

    public int getIntConsole(String msg) throws IOException { //metod to enter some int from console
        int i = 0;
        while (true) {
            System.out.println(msg);
            try (BufferedReader br = new BufferedReader(new InputStreamReader(new UncloseInputStream(System.in)))) {
                i = Integer.valueOf(br.readLine());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Wrong number. Try again.");
            }
        }
        return i;
    }
}
