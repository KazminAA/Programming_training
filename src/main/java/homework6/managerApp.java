package homework6;

/**
 * Created by Alex on 29.08.2016.
 */
public class managerApp {
    public static void main(String[] args) {
        switch (args.length) {
            case 1: {
                miniFileManager.dirList(args[0]);
                break;
            }
            case 2: {
                String command = args[0];
                switch (command) {
                    case "deleteC": {
                        miniFileManager.deleteFile(args[1]);
                        break;
                    }
                    case "forcedelete": {
                        miniFileManager.forcedelete(args[1]);
                        break;
                    }
                    case "mkdir": {
                        miniFileManager.mkDir(args[1]);
                        break;
                    }
                    case "create": {
                        miniFileManager.createFile(args[1]);
                        break;
                    }
                }
                break;
            }
            case 3: {
                if (args[0].equals("rename")) {
                    miniFileManager.renameFile(args[1], args[2]);
                }
                break;
            }
            default: {
                System.out.println("\"managerApp\" - comand list;");
                System.out.println("\"managerApp dirname\" - directory list;");
                System.out.println("\"managerApp deleteC name\" - deleteC 'name' file or directory");
                System.out.println("\"managerApp forcedelete name\" - deleteC 'name' not empty directory");
                System.out.println("\"managerApp mkdir name\" - create 'name' directory");
                System.out.println("\"managerApp create name\" - create 'name' file");
                System.out.println("\"managerApp rename name1 name2\" - rename 'name1' file or directory to name2");
            }
        }
    }
}
