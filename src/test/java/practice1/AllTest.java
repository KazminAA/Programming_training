package practice1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by lex on 12.07.16.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({ArraySumTest.class, ArrayProdTest.class})
public class AllTest {
}
