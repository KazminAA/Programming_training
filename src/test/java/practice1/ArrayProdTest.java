package practice1;

import org.junit.Assert;
import org.junit.Test;

import static practice1.ArrayProd.*;

/**
 * Created by lex on 12.07.16.
 */
public class ArrayProdTest {
    @Test
    public void arrayProdTest() throws Exception {
        double[] dArray = {12, 12.3, 5.1, 16};
        double expected = 12 * 12.3 * 5.1 * 16;
        double actual = arrayProd(dArray);
        Assert.assertEquals("Test for arrayProd static.", expected, actual, 0.01);
    }

    @Test(expected = NullPointerException.class)
    public void arrayProdTestException() throws Exception {
        double[] dArray = {12, 12.3, 5.1, 16};
        double expected = 12 * 12.3 * 5.1 * 16;
        double actual = arrayProd(null);
        Assert.assertEquals("Test for arrayProd static.", expected, actual, 0.01);
    }

}