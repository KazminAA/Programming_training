package practice6.part2;

import java.io.*;

/**
 * Created by lex on 06.09.16.
 */
public class FileTools {
    public static void findFiles(String bDir, String log, String findName) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(log), true))) {
            File beginOfFind = new File(bDir);
            if (!beginOfFind.exists()) {
                System.out.println("Can't find directory.");
                return;
            }
            if (!beginOfFind.isDirectory()) {
                System.out.println(bDir + " is not directory.");
                return;
            }
            File[] fs = beginOfFind.listFiles();
            for (File f : fs) {
                if (f.isDirectory()) {
                    Thread th = new Thread(() -> findFiles(f.getPath(), log, findName));
                    th.run();
                } else {
                    if (f.getName().contains(findName)) {
                        bw.write(f.getAbsolutePath());
                        bw.write('\n');
                        bw.flush();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copyFilesBegin(String from, String to) {
        File copyTo = new File(to);
        if (copyTo.exists() && !copyTo.isDirectory()) {
            System.out.println(to + " is file.");
            return;
        }
        File copyFrom = new File(from);
        if (!copyFrom.exists()) {
            System.out.println(from + " can't find such file or directory.");
            return;
        }
        copyTo.mkdirs();
        if (!copyFrom.isDirectory()) {
            copyFiles(from, to);
        } else {
            directoryList(from, to);
        }
    }

    private static void copyFiles(String from, String to) {
        File copyFrom = new File(from);
        File copyTo = new File(to);
        int i;
        try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(copyFrom));
             BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(copyTo))) {
            while ((i = input.read()) != -1) {
                output.write(i);
            }
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void directoryList(String from, String to) {
        File[] fs = new File(from).listFiles();
        for (int i = 0; i < fs.length; i++) {
            if (fs[i].isDirectory()) {
                File copyTo = new File(to + "/" + fs[i].getName());
                copyTo.mkdirs();
                String inFrom = fs[i].getPath();
                Thread th = new Thread(() -> directoryList(inFrom, copyTo.getPath()));
                th.start();
            } else {
                copyFiles(fs[i].getPath(), to + "/" + fs[i].getName());
            }
        }
    }
}
