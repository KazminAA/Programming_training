package homework2;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by lex on 21.07.16.
 */
public class StringUtils {

    public static String stringTurner(String inString) {
        return (new StringBuilder(inString).reverse().toString());
    }

    public static boolean isPalendrome(String inString) {
        String s = inString.replaceAll(" ", "");
        return s.equalsIgnoreCase(new StringBuilder(s).reverse().toString());
    }

    public static String lenghthLiker(String inString) {
        String s = "";
        if (inString.length() > 10) {
            s = inString.substring(0, 6);
        } else {
            StringBuilder sb = new StringBuilder(inString);
            for (int i = 0; i < (12 - inString.length()); i++) {
                sb.append("o");
            }
            s = sb.toString();
        }
        return s;
    }

    public static String changeWordsInString(String inString) {
        return inString.replaceAll("^([\\s]?)(\\w+)(.*)(\\b\\w+)([.?!]?$)", "$1$4$3$2$5");
    }

    public static String changeWordsInSentence(String inString) {
        StringBuilder sb = new StringBuilder("");
        Pattern p = Pattern.compile("([A-Za-z,\\s]+[.?!])?");
        Matcher m = p.matcher(inString);
        while (m.find()) {
            sb.append(changeWordsInString(m.group()));
        }
        return sb.toString();
    }

    public static boolean charactersCheck(String inString) {
        return inString.matches("[abc]+");
    }

    public static boolean isDate(String inString) {
        return inString.matches("^\\d{2}[\\.]\\d{2}[\\.]\\d{4}$");
    }

    public static boolean isMail(String inString) {
        return inString.matches("\\w+[\\-]?\\w+@\\w+[\\.{1}\\w+]+");
    }

    public static String[] phoneCatcher(String inString) {
        Pattern p = Pattern.compile("\\+\\d{1,2}\\(\\d{3}\\)\\d{3}\\-\\d{2}\\-\\d{2}");
        Matcher m = p.matcher(inString);
        ArrayList<String> retStringList = new ArrayList<>();
        while (m.find()) {
            retStringList.add(m.group());
        }
        String[] s = new String[retStringList.size()];
        retStringList.toArray(s);
        return s;
    }

}
