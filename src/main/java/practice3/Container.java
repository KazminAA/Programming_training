package practice3;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Alex on 16.08.2016.
 */
public class Container<V extends Entity> implements GenericStorage<Long, V>, Iterable<V> {

    private long defKey;
    List<Node<Long, V>> storage = new ArrayList<>();

    public Container(long defKey) {
        setDefKey(defKey);
    }

    public long getDefKey() {
        return defKey;
    }

    public void setDefKey(long defKey) {
        this.defKey = defKey;
    }

    /**
     * Add some Entity to storage if it does not exist
     *
     * @param value Entity to add
     * @return key associated with Entity in storage
     */
    @Override
    public Long add(V value) {
        for (int i = 0; i < storage.size(); i++) {
            if (storage.get(i).getValue().equals(value)) {
                return storage.get(i).getKey();
            }
        }
        long key = getDefKey();
        value.setId(++key);
        storage.add(new Node<>(getDefKey(), value));
        setDefKey(key);
        return key;
    }

    @Override
    public V get(Long key) {
        V value = null;
        for (int i = 0; i < storage.size(); i++) {
            if (storage.get(i).getKey().equals(key)) {
                value = storage.get(i).getValue();
            }
        }
        return value;
    }

    @Override
    public void delete(Long key) {
        for (int i = 0; i < storage.size(); i++) {
            if (storage.get(i).getKey().equals(key)) {
                storage.remove(i);
            }
        }
    }

    @Override
    public void update(Long key, V value) throws IllegallValueExeption {
        for (int i = 0; i < storage.size(); i++) {
            if (storage.get(i).getValue().equals(value)) {
                throw new IllegallValueExeption("Value allready exist!");
            }
        }
        for (int i = 0; i < storage.size(); i++) {
            if (storage.get(i).getKey().equals(key)) {
                storage.get(i).setValue(value);
            }
        }
    }

    @Override
    public Iterator<V> iterator() {
        Iterator<V> iterator = new Iterator<V>() {
            int index = 0;

            @Override
            public boolean hasNext() {
                return storage.size() > index;
            }

            @Override
            public V next() {
                return storage.get(index++).getValue();
            }
        };
        return iterator;
    }

}
