package homework2;

import java.io.IOException;

/**
 * Created by lex on 28.07.16.
 */
public class ParserTest {
    public static void main(String[] args) throws IOException {
        String[] test = StringUtils.phoneCatcher("My number +38(068)605-79-15, Lyuda +38(098)268-56-77");
        for (int i = 0; i < test.length; i++) {
            System.out.println(test[i]);
        }
        String[] sArr = SmallMarkdownParser.readFromFile("text.txt");
        if (sArr != null) {
            System.out.println("~~~~~~~~~~~Original text~~~~~~~~~~~~~~~");
            for (String s : sArr) {
                System.out.println(s);
            }
        }
        System.out.println("");
        String[] s2 = SmallMarkdownParser.parseText(sArr);
        if (s2 != null) {
            System.out.println("~~~~~~~~~~~~text parsed from file~~~~~~~~~~~~~~~");
            for (String s : s2) {
                System.out.println(s);
            }
        }
        System.out.println("");
        s2 = SmallMarkdownParser.readFomString("##Header line\n" +
                "Simple line *with* em\n" +
                "Simple **line** with strong\n" +
                "Line with link [Link to google](https://www.google.com) in center\n" +
                "Line **with** *many* **elements** and *one* link [Link to FB](https://www.facebook.com)");
        if (s2 != null) {
            System.out.println("~~~~~~~~~~~~text parsed from string~~~~~~~~~~~~~~~");
            for (String s : s2) {
                System.out.println(s);
            }
        }
    }
}
