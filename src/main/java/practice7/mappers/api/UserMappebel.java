package practice7.mappers.api;

import practice7.models.Entity;

import java.time.LocalDate;

/**
 * Created by Alexandr on 20.09.2016.
 */
public interface UserMappebel {
    int getId();

    void setId(int id);

    public Boolean getSex();

    public void setSex(Boolean sex);

    public String getPwd();

    public void setPwd(String pwd);

    public LocalDate getBirthday();

    public void setBirthday(LocalDate birthday);

    public String getLogin();

    public void setLogin(String login);

    public String getEmail();

    public void setEmail(String email);

    public String getUserName();

    public void setUserName(String userName);

    public String getUserSurname();

    public void setUserSurname(String userSurname);

    public Entity getRole();

    public void setRole(Entity role);

}
