package practice5;

import java.io.IOException;

/**
 * Created by Alex on 30.08.2016.
 */
public class IODemoApp {
    public static void main(String[] args) {
        IODemo.randomNumberFileGenerator("randomnumber", 10);
        IODemo.sortedIntFromRandom("randomnumber");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        IODemo.studentsAvgAbove(90);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        IODemo.changeWordString("AboutLibrarys.txt");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        IODemo.changeWordSantence("AboutLibrarys.txt");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        try {
            IODemo.copyMetter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Group g1 = new Group(
                new Student("Kirill", 1970, 30),
                new Student("Alexey", 1988, 55),
                new Student("Masha", 1991, 66),
                new Student("Inna", 1986, 68)
        );
        IODemo.groupWrite("testGroup", g1);
        Group g2 = IODemo.groupRead("testGroup");
        g2.getGroup().forEach(System.out::println);
    }
}
