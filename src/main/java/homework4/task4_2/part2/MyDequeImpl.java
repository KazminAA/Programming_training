package homework4.task4_2.part2;

import java.lang.reflect.Field;
import java.util.Iterator;

/**
 * Created by lex on 17.08.16.
 */
public class MyDequeImpl<E> implements MyDeque<E>, Cloneable {
    private int size = 0;
    private Node<E> first;
    private Node<E> last;

    @Override
    public void addFirst(E e) {
        if (size == 0) {
            first = new Node<E>(e);
            last = first;
        } else {
            Node<E> node = new Node<E>(e);
            node.setNext(first);
            first.setPrev(node);
            first = node;
        }
        size++;
    }

    @Override
    public void addLast(E e) {
        if (size == 0) {
            first = new Node<E>(e);
            last = first;
        } else {
            Node<E> node = new Node<E>(e);
            node.setPrev(last);
            last.setNext(node);
            last = node;
        }
        size++;
    }

    @Override
    public E removeFirst() {
        E element = null;
        if (first != null) {
            element = first.getElement();
            if (first.getNext() == null) {
                clear();
            } else {
                first = first.getNext();
                first.setPrev(null);
                size--;
            }
        }
        return element;
    }

    @Override
    public E removeLast() {
        E element = null;
        if (last != null) {
            element = last.getElement();
            if (last.getPrev() == null) {
                clear();
            } else {
                last = last.getPrev();
                last.setNext(null);
                size--;
            }
        }
        return element;
    }

    @Override
    public E getFirst() {
        E element = null;
        if (first != null) element = first.getElement();
        return element;
    }

    @Override
    public E getLast() {
        E element = null;
        if (last != null) element = last.getElement();
        return element;
    }

    @Override
    public boolean contains(Object o) {
        boolean flag = false;
        if (size != 0) {
            Node<E> node = first;
            do {
                if (node.getElement().equals(o)) {
                    flag = true;
                    break;
                } else {
                    node = node.getNext();
                }
            } while (node != null);
        }
        return flag;
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public Object[] toArray() {
        Object[] objects = null;
        if (size != 0) {
            objects = new Object[size];
            int i = 0;
            Node<E> node = first;
            do {
                objects[i] = node.getElement();
                node = node.getNext();
                i++;
            } while (node != null);
        }
        return objects;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean containsAll(MyDeque<? extends E> deque) throws NoSuchFieldException, IllegalAccessException {
        boolean flag = false;
        if (deque == null) return false;
        if (this == deque) return true;
        if (size < deque.size()) return false;
        if (first != null) {
            if (deque.getFirst() == null) return false;
            Node<E> node1 = first;
            E elem = deque.getFirst();
            for (int i = 0; i < size; i++) {
                if (node1.getElement().equals(elem)) {
                    flag = true;
                    break;
                } else {
                    node1 = node1.getNext();
                }
            }
            if (flag) {
                Field f = deque.getClass().getDeclaredField("first");
                f.setAccessible(true);
                Node<E> node2 = (Node<E>) f.get(deque);
                f.setAccessible(false);
                for (int i = 1; i < deque.size(); i++) {
                    node1 = node1.getNext();
                    node2 = node2.getNext();
                    if (!node1.getElement().equals(node2.getElement())) {
                        return false;
                    }
                }
            }
        }
        return flag;
    }

    @Override
    public String toString() {
        String str = null;
        if (first != null) {
            str = "";
            Node<E> node = first;
            int i = 1;
            do {
                str = str + "Element" + i + ": '" + node.getElement().toString() + "'. ";
                node = node.getNext();
                i++;
            } while (node != null);
        }
        return str;
    }

    @Override
    public Iterator<E> iterator() {
        Iterator<E> iterator = new Iterator<E>() {
            private boolean next = false;
            private Node<E> current;

            {
                current = new Node<>(null);
                current.setNext(first);
            }

            @Override
            public boolean hasNext() {
                return current.getNext() != null;
            }

            @Override
            public E next() {
                current = current.getNext();
                next = true;
                return current.getElement();
            }

            @Override
            public void remove() {
                if (!next) {
                    throw new IllegalStateException("Can't remove without nextOrPrev()");
                }
                if (current.getPrev() != null) {
                    current.getPrev().setNext(current.getNext());
                } else {
                    first = current.getNext();
                }
                if (current.getNext() != null) {
                    current.getNext().setPrev(current.getPrev());
                }
                next = false;
                size--;
            }
        };
        return iterator;
    }

    private static class Node<E> {
        private E element;
        private Node<E> prev;
        private Node<E> next;

        public Node(E element) {
            setElement(element);
        }

        public E getElement() {
            return element;
        }

        public void setElement(E element) {
            this.element = element;
        }

        public Node<E> getPrev() {
            return prev;
        }

        public void setPrev(Node<E> prev) {
            this.prev = prev;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }
    }
}
