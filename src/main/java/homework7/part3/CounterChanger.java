package homework7.part3;

/**
 * Created by Alexandr on 06.09.2016.
 */
public class CounterChanger implements Runnable {
    private Counter counter;

    public CounterChanger(Counter counter) {
        setCounter(counter);
    }

    public Counter getCounter() {
        return counter;
    }

    public void setCounter(Counter counter) {
        this.counter = counter;
    }

    public String counterComparison() {
        synchronized (counter) {
            int c1 = counter.getCounter1();
            int c2 = counter.getCounter2();
            String comp = null;
            if (c1 == c2) {
                comp = c1 + "=" + c2;
            }
            if (c1 > c2) {
                comp = c1 + ">" + c2;
            }
            if (c1 < c2) {
                comp = c1 + "<" + c2;
            }
            return comp;
        }
    }

    @Override
    public void run() {
        while (true) {
            //synchronized (counter) {
            System.out.println(Thread.currentThread().getName().concat(" " + counterComparison()));
            counter.incrementC1();
            try {
                Thread.currentThread().sleep(10);
            } catch (InterruptedException e) {
                return;
            }
            counter.incrementC2();
            //}
        }
    }
}
