package practice3;

import static practice3.InMemoryDB.films;

/**
 * Created by Alex on 17.08.2016.
 */
public class TestAppInMemoryDB {
    public static void main(String[] args) throws IllegallValueExeption {
        TestDatabase.crateFilms();
        for (Film f : films) {
            System.out.println(f);
        }
        Film film1 = new Film("Секреты домашних животных(The Secret Life of Pets)", 2016, "комедия / мультфильм / семейный", 1, 27, "Япония, США", "Ярроу Чейни",
                "Мультфильм расскажет о терьере по кличке Макс, чья жизнь переворачивается с ног на голову, когда у его хозяина появляется еще один любимец — дворняжка Дюк. " +
                        "Им приходится забыть о соперничестве и объединить усилия, когда они выясняют, что милый кролик Снежок собирает целую армию брошенных зверей, " +
                        "чтобы отомстить счастливым домашним животным и их хозяевам.",
                "Луис С.К.", "Эрик Стоунстрит", "Кевин Харт", "Дженни Слейт");
        films.update((long) 3, film1);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        for (Film f : films) {
            System.out.println(f);
        }
    }
}
