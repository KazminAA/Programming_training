package practice7.testapp;

import practice7.dto.*;
import practice7.service.impl.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by lex on 13.09.16.
 */
public class App {
    public static void main(String[] args) {
        /*FilmDTO film = new FilmDTO("Стартрек׃ За пределами вселенной(Star Trek Beyond)", 2016, "боевик / приключения / триллер / фантастика", 2, 0, "США", "Джастин Лин",
                "Научно-фантастический боевик погрузит зрителей вместе с командой корабля \"Энтерпрайз\" в космическую бездну новейшие технологические" +
                        " разработки, сверхчувствительные электронные гаджеты и увлекательное путешествие по галактике.", "Крис Пайн", "Закари Куинто", "Саймон Пегг", "Карл Урбан");
        FilmServiceImpl.getInstance().save(film);
        film = new FilmDTO("Отряд самоубийц(Suiside Squad)", 2016, "боевик / криминал / триллер / фантастика / фэнтези", 2, 3, "США", "Дэвид Эйр",
                "Правительство решает дать команде суперзлодеев шанс на искупление. Подвох в том, что их отправляют на миссию, где они вероятнее всего погибнут.",
                "Марго Робби", "Уилл Смит", "Джаред Лето", "Джей Кортни");
        FilmServiceImpl.getInstance().save(film);
        film = new FilmDTO("Охотники за привидениями(Ghostbusters)", 2016, "боевик / комедия / фантастика", 1, 58, "США", "Пол Фиг",
                "Они вернулись, вернулись опять. Всемирно известные охотники за привидениями вновь берутся за старое." +
                        " В полном составе им предстоит сразиться с новой партией призраков и исчадий из ада.", "Крис Хемсворт", "Мелисса МаКарти", "Кейт МакКиннон", "Кристен Уигг");
        FilmServiceImpl.getInstance().save(film);
        film = new FilmDTO("Джейсон Борн(Jason Bourne)", 2016, "боевик / триллер", 2, 3, "США", "Пол Гринграсс",
                "Мир на грани катастрофы, а значит пришло время ему вернуться. Неаполь, Мюнхен, Нью-Йорк — его имя знают повсюду. Элитный" +
                        " суперагент, лучший из лучших, даже в Лас-Вегасе игра пойдет по его правилам. Он — Джейсон Борн.", "Мэтт Дэймон", "Алисия Викандер", "Джулия Стайлз", "Томми Ли Джонс");
        FilmServiceImpl.getInstance().save(film);*/
        List<FilmDTO> films = FilmServiceImpl.getInstance().getAll();
        films.forEach(System.out::println);
        /*int[] structure = {5, 7, 7, 9, 9, 9, 11, 15, 15, 15};
        HallDTO hall = new HallDTO("Красный зал", structure);
        HallServiceImpl.getInstance().save(hall);*/
        /*HallServiceImpl.getInstance().delete(3);*/
        /*int[] structure = {5, 7, 7, 9, 9, 9, 11, 15, 15, 15, 19, 19};
        HallDTO hall = new HallDTO("Красный зал", structure);
        hall.setId(4);
        try {
            HallServiceImpl.getInstance().update(hall);
        } catch (IllegallValueExeption illegallValueExeption) {
            illegallValueExeption.printStackTrace();
        }*/
        List<HallDTO> hallDTOs = HallServiceImpl.getInstance().getAll();
        hallDTOs.forEach(System.out::println);

        /*SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setFilm(FilmServiceImpl.getInstance().getById(8));
        LocalDateTime st = LocalDateTime.of(16,9,19,12,15);
        sessionDTO.setDateOfSeance(st);
        sessionDTO.setHall(HallServiceImpl.getInstance().getById(6));
        sessionDTO.setPrice(BigDecimal.valueOf(100.50));
        SessionServiceImpl.getInstance().save(sessionDTO);*/
        List<SessionDTO> sessionDTOs = SessionServiceImpl.getInstance().getAll();
        sessionDTOs.forEach(System.out::println);

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        /*HallDTO hallDTO = HallServiceImpl.getInstance().getById(6);
        System.out.println(hallDTO);
        List<SessionDTO> sessionDTOs2 = SessionServiceImpl.getInstance().getSessionsByFK("filmID", "7");
        sessionDTOs2.forEach(System.out::println);*/

       /* List<UserDTO> userDTOs = UserServiceImpl.getInstance().getAll();
        userDTOs.forEach(System.out::println);*/

        LocalDateTime begin = LocalDateTime.now();
        LocalDateTime end = begin.plusDays(3);
        List<SessionDTO> sessionDTOs1 = SessionServiceImpl.getInstance().getSessionBetween(begin, end);
        sessionDTOs1.forEach(System.out::println);

        /*TicketDTO ticketDTO = new TicketDTO(5, 6, false, true, begin, 4, 3);
        TicketServiceImpl.getInstance().save(ticketDTO);
        ticketDTO = new TicketDTO(5, 7, false, true, begin, 4, 3);
        TicketServiceImpl.getInstance().save(ticketDTO);
        ticketDTO = new TicketDTO(5, 8, false, true, begin, 2, 3);
        TicketServiceImpl.getInstance().save(ticketDTO);
        ticketDTO = new TicketDTO(6, 8, false, true, begin, 3, 2);
        TicketServiceImpl.getInstance().save(ticketDTO);
        ticketDTO = new TicketDTO(6, 7, false, true, begin, 3, 2);
        TicketServiceImpl.getInstance().save(ticketDTO);*/
        List<TicketDTO> ticketDTOs = TicketServiceImpl.getInstance().getAll();
        ticketDTOs.forEach(System.out::println);
    }
}
