package practice7.service.impl;

import practice3.IllegallValueExeption;
import practice7.dao.DaoFactory;
import practice7.dao.api.Dao;
import practice7.dto.TicketDTO;
import practice7.mappers.BeanMapper;
import practice7.models.Ticket;
import practice7.service.api.Service;

import java.util.List;

/**
 * Created by Alexandr on 22.09.2016.
 */
public class TicketServiceImpl implements Service<TicketDTO> {
    private static TicketServiceImpl ticketService;

    private BeanMapper beanMapper;
    private Dao<Ticket> ticketDao;

    private TicketServiceImpl() {
        this.beanMapper = BeanMapper.getInstance();
        this.ticketDao = DaoFactory.getInstance().getTicketDao();
    }

    public static synchronized TicketServiceImpl getInstance() {
        if (ticketService == null) {
            ticketService = new TicketServiceImpl();
        }
        return ticketService;
    }

    @Override
    public List<TicketDTO> getAll() {
        List<Ticket> tickets = ticketDao.getAll();
        List<TicketDTO> ticketDTOs = beanMapper.listMapToList(tickets, TicketDTO.class);
        return ticketDTOs;
    }

    @Override
    public TicketDTO getById(int id) {
        Ticket ticket = ticketDao.getById(id);
        TicketDTO result = null;
        if (ticket != null) {
            result = beanMapper.singleMapper(ticket, TicketDTO.class);
        }
        return result;
    }

    @Override
    public void save(TicketDTO entity) {
        Ticket ticket = beanMapper.singleMapper(entity, Ticket.class);
        ticketDao.save(ticket);
    }

    @Override
    public void delete(int id) {
        ticketDao.delete(id);
    }

    @Override
    public void update(TicketDTO entity) throws IllegallValueExeption {
        Ticket ticket = beanMapper.singleMapper(entity, Ticket.class);
        ticketDao.update(ticket);
    }
}
