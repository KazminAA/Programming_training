package homework4.task4_2.part3;

import java.util.Iterator;

/**
 * Created by Alex on 19.08.2016.
 */
public interface ListIterator<E> extends Iterator<E> {
    // проверяет, есть ли предыдущий элемент для выборки методом previous

    boolean hasPrevious();

// возвращает предыдущий элемент

    E previous();

// заменяет элемент, который на предыдущем шаге был возвращен nextOrPrev/previous на данный элемент

    void set(E e);

    void newUse();

}
