package homework6;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

/**
 * Created by Alex on 29.08.2016.
 */
public class App {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        List<Book> books = Book.createBooksFromFile("Books.txt");
        books.forEach(System.out::println);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        books.get(2).bookToFile("Book2.txt");
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("Book2.txt"));
        Book newBook = (Book) in.readObject();
        in.close();
        System.out.println(newBook);
    }

}
