package practice3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 16.08.2016.
 */
public class Cinema extends Entity {
    private List<User> users = new ArrayList<>();
    private List<Hall> halls = new ArrayList<>();

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Hall> getHalls() {
        return halls;
    }

    public void setHalls(List<Hall> halls) {
        this.halls = halls;
    }

    public void addUser (User user) {
        this.users.add(user);
    }
}
