package homework7.part3;

/**
 * Created by Alexandr on 06.09.2016.
 */
public class App {
    public static void main(String[] args) {
        Counter counter = new Counter(1, 1);
        Thread th1 = new Thread(new CounterChanger(counter), "Changer1");
        Thread th2 = new Thread(new CounterChanger(counter), "Changer2");
        Thread th3 = new Thread(new CounterChanger(counter), "Changer3");
        Thread th4 = new Thread(new CounterChanger(counter), "Changer4");
        th1.start();
        th2.start();
        th3.start();
        th4.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        th1.interrupt();
        th2.interrupt();
        th3.interrupt();
        th4.interrupt();

    }
}
