package practice4;

import java.io.IOException;
import java.util.Map;

import static practice4.sortMethod.*;

/**
 * Created by Alex on 22.08.2016.
 */
public class MeterClassDemo {
    public static void main(String[] args) throws IOException {
        Map<String, Integer> wm = WordsMeterClass.wordsMeter("Romeo.txt");
        for (Map.Entry<String, Integer> entry : wm.entrySet()) {
            System.out.println(entry.getKey() + "->" + entry.getValue());
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        wm = WordsMeterClass.wordsMeter("Romeo.txt", WORD_HL);
        for (Map.Entry<String, Integer> entry : wm.entrySet()) {
            System.out.println(entry.getKey() + "->" + entry.getValue());
        }
    }
}
