package practice1;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static practice1.ArraySum.*;

/**
 * Created by lex on 12.07.16.
 */
public class ArraySumTest {
    public static double[] testArray = null;

    @BeforeClass
    public static void initTestArray() {
        double[] dTest = {12, 12.3, 5.1, 16};
        testArray = dTest;
    }

    @Test
    public void sumStatic() throws Exception {
        double expected = 12 + 12.3 + 5.1 + 16;
        double actual = sum(testArray);
        System.out.println(testArray.hashCode());
        Assert.assertEquals("Test for Sum static.", expected, actual, 0.01);
    }

    @Test
    public void sumFromConstructor() throws Exception {
        double expected = 12 + 12.3 + 5.1 + 16;
        double actual = new ArraySum(testArray).sum();
        System.out.println(testArray.hashCode());
        Assert.assertEquals("Test for Sum from Constructor.", expected, actual, 0.01);
    }

    @Test(expected = NullPointerException.class)
    public void sumException() throws Exception {
        double expected = 12 + 12.3 + 5.1 + 16;
        double actual = sum(null);
        Assert.assertEquals("Test for Sum from Constructor.", expected, actual, 0.01);
    }

}