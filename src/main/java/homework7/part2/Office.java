package homework7.part2;

/**
 * Created by Alexandr on 06.09.2016.
 */
public class Office {
    private String firm;
    private Fax fax;

    public Office(String firm, int phaxNumber) {
        this.firm = firm;
        this.fax = new Fax(phaxNumber);
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public Fax getFax() {
        return fax;
    }

    @Override
    public String toString() {
        return "{" +
                "'" + firm + '\'' +
                ", fax=" + fax +
                '}';
    }

    public class Fax {
        private int number;

        public Fax(int number) {
            setNumber(number);
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        @Override
        public String toString() {
            return Integer.toString(number);
        }

        public synchronized void sendFax(Office from, Office to) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            to.getFax().getFax(from);
        }

        public synchronized void getFax(Office from) {
            System.out.println("Get fax from: " + from);
        }
    }
}
