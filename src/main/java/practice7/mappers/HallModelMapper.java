package practice7.mappers;

import practice7.mappers.api.HallMappeble;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Alexandr on 19.09.2016.
 */
public class HallModelMapper {
    public static <T extends HallMappeble> T hallObjMap(HallMappeble hall, Class<T> toClass) {
        T result = null;
        try {
            result = toClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        result.setId(hall.getId());
        result.setName(hall.getName());
        int len = hall.getStructure().length;
        int[] Arr = new int[len];
        System.arraycopy(hall.getStructure(), 0, Arr, 0, len);
        result.setStructure(Arr);
        return result;
    }

    public static <E extends HallMappeble, T extends HallMappeble> List<T> hallListMap(Iterable<E> entity,
                                                                                       Class<T> toClass) {
        List<T> result = new LinkedList<>();
        for (E e : entity) {
            result.add(hallObjMap(e, toClass));
        }
        return result;
    }
}
