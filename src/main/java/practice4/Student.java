package practice4;

/**
 * Created by lex on 19.08.16.
 */
public class Student {
    private String name;
    private String suname;
    private int course;

    public Student(String name, String suname, int course) {
        setName(name);
        setSuname(suname);
        setCourse(course);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSuname() {
        return suname;
    }

    public void setSuname(String suname) {
        this.suname = suname;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Student{" +
                "suname='" + suname + '\'' +
                ", name='" + name + '\'' +
                ", course=" + course +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;

        Student student = (Student) o;

        if (!getName().equals(student.getName())) return false;
        return getSuname().equals(student.getSuname());

    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getSuname().hashCode();
        return result;
    }
}
