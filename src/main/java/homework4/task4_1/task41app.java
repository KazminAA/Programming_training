package homework4.task4_1;

/**
 * Created by Alex on 17.08.2016.
 */
public class task41app {
    public static void main(String[] args) {
        Integer[] i = {12, 10, 3, 18, 10, 60};
        System.out.println(maxElement(i));
        String[] s = {"Cebab", "Arab", "Baobab"};
        System.out.println(maxElement(s));
        Computer[] c = {
                new Computer("Intel Core i3-6100 3.7GHz/3Mb", "B150 GigaByte GA-B150-HD3 ATX", 1),
                new Computer("Intel Core i3-6100 3.7GHz/3Mb", "B150 GigaByte GA-B150-HD3 ATX", 2),
                new Computer("Intel Core i3-6100 3.7GHz/3Mb", "B150 GigaByte GA-B150-HD3 ATX", 4),
                new Computer("Intel Core i3-6100 3.7GHz/3Mb", "B150 GigaByte GA-B150-HD3 ATX", 8)
        };
        System.out.println(maxElement(c));
        Car[] car = {
                new Car("BMW", "X5", 2009),
                new Car("Mercedes", "Vito", 2003),
                new Car("BMW", "X5", 2016),
        };
        //System.out.println(maxElement(car));
    }
    public static Object maxElement(Comparable[] c) {
        Object max = c[0];
        for (int i = 1; i < c.length; i++) {
            if (c[i].compareTo(max) > 0) max = c[i];
        }
        return max;
    }
}
