package homework7.part2;

/**
 * Created by Alexandr on 06.09.2016.
 */
public class App {
    public static void main(String[] args) {
        Office of1 = new Office("Bagira", 325645);
        Office of2 = new Office("Akella", 125644);
        Thread th1 = new Thread(new FaxSending(of1, of2));
        Thread th2 = new Thread(new FaxSending(of2, of1));
        th1.start();
        th2.start();
    }
}
