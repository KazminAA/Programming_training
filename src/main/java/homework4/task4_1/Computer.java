package homework4.task4_1;

/**
 * Created by Alex on 17.08.2016.
 */
public class Computer implements Comparable{
    private String processor = "";
    private String motheboard = "";
    private int ramGb = 0;

    public Computer(String processor, String motheboard, int ramGb) {
        setProcessor(processor);
        setMotheboard(motheboard);
        setRamGb(ramGb);
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getMotheboard() {
        return motheboard;
    }

    public void setMotheboard(String motheboard) {
        this.motheboard = motheboard;
    }

    public int getRamGb() {
        return ramGb;
    }

    public void setRamGb(int ramGb) {
        this.ramGb = ramGb;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "processor='" + processor + '\'' +
                ", motheboard='" + motheboard + '\'' +
                ", ramGb=" + ramGb +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        if (o == null) return 1;
        return getProcessor().compareTo(((Computer) o).getProcessor()) +
                getMotheboard().compareTo(((Computer) o).getMotheboard()) +
                (getRamGb() - ((Computer) o).getRamGb());
    }
}
