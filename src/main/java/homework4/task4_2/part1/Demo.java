package homework4.task4_2.part1;

import java.util.Arrays;

/**
 * Created by lex on 17.08.16.
 */
public class Demo {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addFirst(8.8);
        deque.addLast(10);
        deque.addFirst(11);
        deque.addLast(5.12);
        deque.addLast(3);
        System.out.println("Size of deque: " + deque.size());
        System.out.println("Is deque contains 1? " + deque.contains(1));
        System.out.println("Is deque contains 5.12? " + deque.contains(5.12));
        System.out.println(deque);
        System.out.println("First element: " + deque.getFirst());
        System.out.println("Last element: " + deque.getLast());
        System.out.println(deque.removeFirst());
        System.out.println(deque);
        System.out.println(deque.removeLast());
        System.out.println(deque);
        Object[] objects = deque.toArray();
        System.out.println(Arrays.toString(objects));
        MyDeque<Number> deque1 = deque;
        System.out.println(deque.containsAll(deque1));
        deque1 = new MyDequeImpl<>();
        deque1.addFirst(8.8);
        deque1.addLast(10);
        deque1.addLast(1);
        System.out.println(deque.containsAll(deque1));
        deque1.removeLast();
        System.out.println(deque.containsAll(deque1));
    }
}
