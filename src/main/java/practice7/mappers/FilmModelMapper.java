package practice7.mappers;

import practice7.mappers.api.FilmMappeble;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Alexandr on 19.09.2016.
 */
public class FilmModelMapper {
    public static <T extends FilmMappeble> T filmObjMap(FilmMappeble film, Class<T> toClass) {
        T outFilm = null;
        try {
            outFilm = toClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        outFilm.setId(film.getId());
        outFilm.setName(film.getName());
        outFilm.setYearOfRelease(film.getYearOfRelease());
        outFilm.setDurationMin(film.getDurationMin());
        outFilm.setGenre(film.getGenre());
        outFilm.setCountry(film.getCountry());
        outFilm.setProduser(film.getProduser());
        outFilm.setCast(film.getCast());
        outFilm.setDescription(film.getDescription());
        return outFilm;
    }

    public static <E extends FilmMappeble, T extends FilmMappeble> List<T> filmListMap(Iterable<E> entity,
                                                                                       Class<T> toClass) {
        List<T> result = new LinkedList<>();
        for (E e : entity) {
            result.add(filmObjMap(e, toClass));
        }
        return result;
    }
}
