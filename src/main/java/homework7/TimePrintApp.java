package homework7;

import homework1.UncloseInputStream;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Alexandr on 05.09.2016.
 */
public class TimePrintApp {
    public static void main(String[] args) {
        TimePrint tp = new TimePrint();
        tp.start();
        Thread th = new Thread(new TimePrintR());
        th.start();
        try (InputStreamReader in = new InputStreamReader(new UncloseInputStream(System.in))) {
            if (in.read() != -1) {
                tp.interrupt();
                th.interrupt();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
