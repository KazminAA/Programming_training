package homework1;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by lex on 11.07.16.
 */
public class DateTest {
    public static void main(String[] args) throws IOException {
        Date d1 = new Date(2016, 07, 13);
        //d1.setMonthByName("DECEMBE");
        Date d2 = new Date(2016, 07, 01);
        Date d3 = new Date(2013, 05, 06);
        System.out.println(d1);
        System.out.println(d2);
        System.out.println(d1.daysBetween(d2));
        System.out.println(d3);
        Date[] dateArray = {d1, d2, d3};
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        for (Date d : dateArray) {
            System.out.println(d);
        }
        System.out.println("~~~~~~~sorted~~~~~~~~~~~~~~~~");
        Arrays.sort(dateArray);
        for (Date d : dateArray) {
            System.out.println(d);
        }
        Car car1 = new Car("BMW", "X5", 2009) {
            @Override
            public boolean equals(Object obj) {
                if (obj == null) return false;
                if (!(obj instanceof Car)) return false;
                return (this.getModel() == ((Car) obj).getModel()) && (this.getMark() == ((Car) obj).getMark());
            }

            @Override
            public String toString() {
                return "{\"" + this.getMark() + "\" model: " + this.getModel() + " year of issue: " + this.getYear() + "}";
            }
        };
        Car car2 = new Car("Mercedes", "Vito", 2003) {
            @Override
            public String toString() {
                return this.getMark() + " " + this.getModel() + " " + this.getYear();
            }
        };
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(car1);
        System.out.println(car2);
        Car car3 = new Car("BMW", "X5", 2009);
        System.out.println(car1.equals(d1));
        System.out.println(car1.equals(car2));
        System.out.println(car1.equals(car3));
    }
}
